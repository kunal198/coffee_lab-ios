//
//  SaveImage.swift
//  CoffeLab
//
//  Created by Brst on 31/08/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit
import Photos
class SaveImage: NSObject {

    var assetCollection: PHAssetCollection!
    var imageToSave = UIImage()
    var VideoToSave = NSURL()
    var requestType = String()
    var isVerifyAuth = Bool()
    
    static let albumName = "Till Printout"
    
    class var sharedInstance: SaveImage {
        
        struct Static {
            static let instance: SaveImage = SaveImage()
        }
        
        return Static.instance
    }
    
    
    
    override init() {
        super.init()
        
    }
    
    func checkFolder() {
        if let assetCollection = fetchAssetCollectionForAlbum() {
            self.assetCollection = assetCollection
            return
        }
        
        if PHPhotoLibrary.authorizationStatus() != PHAuthorizationStatus.authorized {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
                ()
            })
        }
        
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            
            if(self.requestType == "image"){
                self.isVerifyAuth = false
            }
            else if(self.requestType == "video"){
                self.isVerifyAuth = true
            }
            self.createAlbum()
        } else {
            PHPhotoLibrary.requestAuthorization(requestAuthorizationHandler)
        }
        
    }
    
    
    func requestAuthorizationHandler(status: PHAuthorizationStatus) {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            
            self.isVerifyAuth = true
            self.createAlbum()
        } else {
            print("should really prompt the user to let them know it's failed")
        }
    }
    
    func createAlbum() {
        PHPhotoLibrary.shared().performChanges({
            PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: SaveImage.albumName)
            // create an asset collection with the album name
        }) { success, error in
            if success {
                self.assetCollection = self.fetchAssetCollectionForAlbum()
                if(self.isVerifyAuth){
                    if(self.requestType == "image"){
                        self.save(image: self.imageToSave)
                    }
                    else if(self.requestType == "video"){
                        self.saveVideo(videoPath: self.VideoToSave as URL)
                    }
                }
            }else {
                print("error \(String(describing: error))")
            }
        }
    }
    
    func fetchAssetCollectionForAlbum() -> PHAssetCollection? {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", SaveImage.albumName)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        
        if let _: AnyObject = collection.firstObject {
            return collection.firstObject
        }
        
        return nil
    }
    
    func save(image: UIImage) {
        
        if assetCollection == nil {
            // if there was an error upstream, skip the save
        }
        imageToSave = image
        requestType = "image"
        self.checkFolder()
        
        PHPhotoLibrary.shared().performChanges({
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            let assetPlaceHolder = assetChangeRequest.placeholderForCreatedAsset
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
            let enumeration: NSArray = [assetPlaceHolder!]
            albumChangeRequest!.addAssets(enumeration)
            
        }, completionHandler: nil)
    }
    
    func saveVideo(videoPath: URL) {
        
        if assetCollection == nil {
            // if there was an error upstream, skip the save
        }
        VideoToSave = videoPath as NSURL
        requestType = "video"
        self.checkFolder()
        
        PHPhotoLibrary.shared().performChanges({
            let asss = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoPath)
            let assetPlaceHolder = asss?.placeholderForCreatedAsset
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
            let enumeration: NSArray = [assetPlaceHolder!]
            albumChangeRequest!.addAssets(enumeration)
            
        }, completionHandler: nil)
    }
}
