//
//  TemperatureViewController.swift
//  CoffeLab
//
//  Created by Brst on 21/08/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class TemperatureViewController: UIViewController,UIPickerViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var submitButton = UIButton()
    @IBOutlet weak var backButton = UIButton()
     @IBOutlet weak var locDropDownButton = UIButton()
     @IBOutlet weak var milkDropDownButton = UIButton()
    @IBOutlet weak var backIcon = UIImageView()
    
    @IBOutlet weak var locationLine = UILabel()
    @IBOutlet weak var delivertyLine = UILabel()
    @IBOutlet weak var milkLine = UILabel()
    @IBOutlet weak var useByLine = UILabel()
    @IBOutlet weak var tempLine = UILabel()
    @IBOutlet weak var quantityLine = UILabel()
    @IBOutlet weak var locationTxt = UITextField()
    @IBOutlet weak var timeDeliveryTxt = UITextField()
    @IBOutlet weak var milkTxt = UITextField()
    @IBOutlet weak var useByTxt = UITextField()
    @IBOutlet weak var tempTxt = UITextField()
    @IBOutlet weak var quantityTxt = UITextField()
    var fieldTagVal = Int()
    var doneButton: UIBarButtonItem!
    var pickOption :NSMutableArray = NSMutableArray()
    let pickerView = UIPickerView()
    
    var locationItemArr = NSMutableArray()
    var milkItemArr = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        submitButton?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
        submitButton?.layer.borderWidth = 1.5
        submitButton?.layer.cornerRadius = 20
        
        backIcon?.image = backIcon?.image!.withRenderingMode(.alwaysTemplate)
        backIcon?.tintColor = UIColor (colorLiteralRed: 66/255, green: 0/255, blue: 0/255, alpha: 1)
        pickerView.delegate = self
        
        locationTxt?.delegate = self
        timeDeliveryTxt?.delegate = self
        milkTxt?.delegate = self
        useByTxt?.delegate = self
        tempTxt?.delegate = self
        quantityTxt?.delegate = self
        
        locationTxt?.inputView = pickerView
        milkTxt?.inputView = pickerView
        locationTxt?.tag = 0
        timeDeliveryTxt?.tag = 1
        milkTxt?.tag = 2
        useByTxt?.tag = 3
        tempTxt?.tag = 4
        quantityTxt?.tag = 5
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 250/255, green: 150/255, blue: 10/255, alpha: 1)
        doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(TemperatureViewController.donePressed))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(TemperatureViewController.cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
         pickerView.backgroundColor = UIColor .white
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        locationTxt?.inputAccessoryView = toolBar
        milkTxt?.inputAccessoryView = toolBar
        
        for i in 0 ..< 10
        {
            let val = String(i)
            locationItemArr.add("Greenlane"+val)
            milkItemArr.add("Trim"+val)
        }

    }
    func donePressed (sender: UIButton) {
        locationTxt?.isUserInteractionEnabled = true
        if fieldTagVal == 0
        {
            locationTxt?.text = pickOption[sender.tag] as? String
        }
        else
        {
            milkTxt?.text = pickOption[sender.tag] as? String
        }
        
        view.endEditing(true)
        
    }
    
    func cancelPressed() {
        locationTxt?.isUserInteractionEnabled = true
        view.endEditing(true) // or do something
    }
    public func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
        
    }
    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        // print( "The value is\(pickOption[row])")
//        return pickOption[row] as? String
//    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: (pickOption[row] as? String)!, attributes: [NSForegroundColorAttributeName : UIColor (colorLiteralRed: 66/255, green: 0/255, blue: 0/255, alpha: 1)])
        return attributedString
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        doneButton.tag = row
    }
    func uderLinesHeight()
    {
        locationLine?.frame.size.height = 1
        delivertyLine?.frame.size.height = 1
        milkLine?.frame.size.height = 1
        useByLine?.frame.size.height = 1
        tempLine?.frame.size.height = 1
        quantityLine?.frame.size.height = 1
    }
    @IBAction func submitButtonAction(sender : UIButton)
    {
        
    }
    @IBAction func locDropDownAction(sender : UIButton)
    {
        
    }
    @IBAction func milkDropDownAction(sender : UIButton)
    {
        
    }
    @IBAction func backButtonAction(sender : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 0
        {
            fieldTagVal = 0
            pickOption = NSMutableArray(array: locationItemArr)
            
            
            locationLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 1
        {
            delivertyLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 2
        {
            pickerView.reloadAllComponents()
            pickerView.selectedRow(inComponent: 0)
            fieldTagVal = 1
            pickOption = NSMutableArray(array: milkItemArr)
            milkLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 3
        {
            useByLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 4
        {
            tempLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 5
        {
            quantityLine?.backgroundColor = UIColor.lightGray
        }
        pickerView.reloadAllComponents()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //pickOption.removeAllObjects()
        if textField.tag == 0
        {
            locationLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
            
        }
       else if textField.tag == 1
        {
            delivertyLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }

        else if textField.tag == 2
        {
            milkLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 3
        {
            useByLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 4
        {
            tempLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 5
        {
            quantityLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        
        if textField.tag == 2
        {}
        else
        {
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
