//
//  CoffeeCountViewController.swift
//  CoffeLab
//
//  Created by Brst on 21/08/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class CoffeeCountViewController: UIViewController,UITextFieldDelegate,UIScrollViewDelegate,UIPickerViewDelegate {
    @IBOutlet weak var countButton = UIButton()
    @IBOutlet weak var backButton = UIButton()
    @IBOutlet weak var submitButton = UIButton()
    @IBOutlet weak var backIcon = UIImageView()
    @IBOutlet weak var itemsScrollView = UIScrollView()
    
    @IBOutlet weak var locationTxt = UITextField()
    @IBOutlet weak var timeDeliveryTxt = UITextField()
    
    @IBOutlet weak var productFirstTxt = UITextField()
    @IBOutlet weak var productSecTxt = UITextField()
    @IBOutlet weak var productThirdTxt = UITextField()
    @IBOutlet weak var productFourthTxt = UITextField()
    @IBOutlet weak var productFifthTxt = UITextField()
    @IBOutlet weak var productSixthTxt = UITextField()
    @IBOutlet weak var productTotalTxt = UITextField()
    
    @IBOutlet weak var locationUndLine = UILabel()
    @IBOutlet weak var timeDeliveryUndLine = UILabel()
    @IBOutlet weak var productFirstLine = UILabel()
    @IBOutlet weak var productSecLine = UILabel()
    @IBOutlet weak var productThirdLine = UILabel()
    @IBOutlet weak var productFourthLine = UILabel()
    @IBOutlet weak var productFifthLine = UILabel()
    @IBOutlet weak var productSixthLine = UILabel()
    @IBOutlet weak var productTotalLine = UILabel()
    
    @IBOutlet weak var headerLabel = UILabel()
    var fieldTagVal = Int()
    var doneButton: UIBarButtonItem!
    var pickOption :NSMutableArray = NSMutableArray()
     let pickerView = UIPickerView()
    
    var locationItemArr = NSMutableArray()
    var timeDeliveryItemArr = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        headerLabel?.text = "Coffee Count"
        countButton?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
        countButton?.layer.borderWidth = 1.5
        countButton?.layer.cornerRadius = 20
        submitButton?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
        submitButton?.layer.borderWidth = 1.5
        submitButton?.layer.cornerRadius = 20
        itemsScrollView?.delegate = self
        
        backIcon?.image = backIcon?.image!.withRenderingMode(.alwaysTemplate)
        backIcon?.tintColor = UIColor (colorLiteralRed: 66/255, green: 0/255, blue: 0/255, alpha: 1)
        itemsScrollView?.isHidden = true
        productFirstTxt?.delegate = self
        productSecTxt?.delegate = self
        productThirdTxt?.delegate = self
        productFourthTxt?.delegate = self
        productFifthTxt?.delegate = self
        productSixthTxt?.delegate = self
        productTotalTxt?.delegate = self
        locationTxt?.delegate = self
        timeDeliveryTxt?.delegate = self
        productFirstTxt?.tag = 0
        productSecTxt?.tag = 1
        productThirdTxt?.tag = 2
        productFourthTxt?.tag = 3
        productFifthTxt?.tag = 4
        productSixthTxt?.tag = 5
        productTotalTxt?.tag = 6
        
        locationTxt?.tag = 7
        timeDeliveryTxt?.tag = 8
        
        self.itemsScrollView?.contentSize = CGSize(width: self.view.frame.size.width,height: (self.itemsScrollView?.frame.size.height)!)
        
       
        pickerView.delegate = self
        locationTxt?.inputView = pickerView
        timeDeliveryTxt?.inputView = pickerView

        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 250/255, green: 150/255, blue: 10/255, alpha: 1)
        doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(CoffeeCountViewController.donePressed))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CoffeeCountViewController.cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
         pickerView.backgroundColor = UIColor .white
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        locationTxt?.inputAccessoryView = toolBar
        timeDeliveryTxt?.inputAccessoryView = toolBar
        
        
        for i in 0 ..< 10
        {
            let val = String(i)
            locationItemArr.add("Greenlane"+val)
            timeDeliveryItemArr.add("AM"+val)
        }
        
    }
    func donePressed (sender: UIButton) {
        locationTxt?.isUserInteractionEnabled = true
        if fieldTagVal == 7
        {
            locationTxt?.text = pickOption[sender.tag] as? String
        }
        else
        {
            timeDeliveryTxt?.text = pickOption[sender.tag] as? String
        }
        
        view.endEditing(true)
        
    }
    
    func cancelPressed() {
        locationTxt?.isUserInteractionEnabled = true
        view.endEditing(true) // or do something
    }
    public func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: (pickOption[row] as? String)!, attributes: [NSForegroundColorAttributeName : UIColor (colorLiteralRed: 66/255, green: 0/255, blue: 0/255, alpha: 1)])
        
        //attributedString.font = UIFont.init(name: attributedString, size: size)
        return attributedString
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        doneButton.tag = row
    }
    @IBAction func countButtonAction(sender : UIButton)
    {
        itemsScrollView?.isHidden = false
        headerLabel?.text = "Bean Count"
    }
    @IBAction func submitButtonAction(sender : UIButton)
    {
        itemsScrollView?.isHidden = true
        headerLabel?.text = "Coffee Count"
    }
    @IBAction func backButtonAction(sender : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
         pickOption.removeAllObjects()
        
        if textField.tag == 0
        {
            productFirstLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 1
        {
            productSecLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 2
        {
            productThirdLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 3
        {
            productFourthLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 4
        {
            productFifthLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 5
        {
            productSixthLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 6
        {
            productTotalLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 7
        {
            fieldTagVal = 7
             pickOption = NSMutableArray(array: locationItemArr)
        
            locationUndLine?.backgroundColor = UIColor.lightGray
    
        }
        else if textField.tag == 8
        {
            fieldTagVal = 8
            pickOption = NSMutableArray(array: timeDeliveryItemArr)
    
    timeDeliveryUndLine?.backgroundColor = UIColor.lightGray
            
        }
        pickerView.selectedRow(inComponent: 0)
        pickerView.reloadAllComponents()
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 0
        {
            productFirstLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 1
        {
            productSecLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 2
        {
            productThirdLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 3
        {
            productFourthLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 4
        {
            productFifthLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 5
        {
            productSixthLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 6
        {
            productTotalLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 7
        {
            locationUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 8
        {
            timeDeliveryUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
    
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        
        if textField.tag == 7
        {}
        else if textField.tag == 8
        {}
        else
        {
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
