//
//  CashUpViewController.swift
//  CoffeLab
//
//  Created by Brst on 21/08/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class CashUpViewController: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIScrollViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var countButton = UIButton()
    @IBOutlet weak var contentView = UIView()
    @IBOutlet weak var notesButton = UIButton()
    @IBOutlet weak var notesUnderLine = UILabel()
    @IBOutlet weak var coinsButton = UIButton()
    @IBOutlet weak var coinsUnderLine = UILabel()
    @IBOutlet weak var totalButton = UIButton()
    @IBOutlet weak var totalUnderLine = UILabel()
    @IBOutlet weak var submitButton = UIButton()
    
    @IBOutlet weak var productOneLbl = UILabel()
    @IBOutlet weak var productSecondLbl = UILabel()
    @IBOutlet weak var productThirdLbl = UILabel()
    @IBOutlet weak var productFourthLbl = UILabel()
    @IBOutlet weak var productFifthLbl = UILabel()
    @IBOutlet weak var productSixthLbl = UILabel()
    
    @IBOutlet weak var productOneTxt = UITextField()
    @IBOutlet weak var productSecondTxt = UITextField()
    @IBOutlet weak var productThirdTxt = UITextField()
    @IBOutlet weak var productFourthTxt = UITextField()
    @IBOutlet weak var productFifthTxt = UITextField()
    @IBOutlet weak var saveButton = UIButton()
    
    @IBOutlet weak var productOneUndLine = UILabel()
    @IBOutlet weak var productSecondUndLine = UILabel()
    @IBOutlet weak var productThirdUndLine = UILabel()
    @IBOutlet weak var productFourthUndLine = UILabel()
    @IBOutlet weak var productFifthUndLine = UILabel()
    @IBOutlet weak var productSixthUndLine = UILabel()
    
    @IBOutlet weak var saveDropDownButton = UIButton()
    @IBOutlet weak var backButton = UIButton()
    @IBOutlet weak var backIcon = UIImageView()
    
    @IBOutlet weak var locationTxt = UITextField()
    @IBOutlet weak var terminalTxt = UITextField()
    @IBOutlet weak var locationUndLine = UILabel()
    @IBOutlet weak var terminalUndLine = UILabel()
    
    @IBOutlet weak var headerLabel = UILabel()
    var fieldTagVal = Int()
    var doneButton: UIBarButtonItem!
    var pickOption :NSMutableArray = NSMutableArray()
    
    var locationItemArr = NSMutableArray()
    var terminalItemArr = NSMutableArray()
     var saveItemArr = NSMutableArray()
    let pickerView = UIPickerView()
     var clickedImg = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView?.isHidden = true
        countButton?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
        countButton?.layer.borderWidth = 1.5
        countButton?.layer.cornerRadius = 20
        submitButton?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
        submitButton?.layer.borderWidth = 1.5
        submitButton?.layer.cornerRadius = 20
        
        saveButton?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
        saveButton?.layer.borderWidth = 1
        saveButton?.layer.cornerRadius = 15
        
        coinsUnderLine?.isHidden = true
        totalUnderLine?.isHidden = true
        saveButton?.isHidden = true
        backIcon?.image = backIcon?.image!.withRenderingMode(.alwaysTemplate)
        backIcon?.tintColor = UIColor (colorLiteralRed: 66/255, green: 0/255, blue: 0/255, alpha: 1)
        
        productOneTxt?.tag = 0
        productSecondTxt?.tag = 1
        productThirdTxt?.tag = 2
        productFourthTxt?.tag = 3
        productFifthTxt?.tag = 4
        locationTxt?.tag = 6
        terminalTxt?.tag = 7
        
        productOneTxt?.delegate = self
        productSecondTxt?.delegate = self
        productThirdTxt?.delegate = self
        productFourthTxt?.delegate = self
        productFifthTxt?.delegate = self
         locationTxt?.delegate = self
         terminalTxt?.delegate = self
        
        pickerView.delegate = self
        locationTxt?.inputView = pickerView
        terminalTxt?.inputView = pickerView
       // productSixthTxt?.inputView = pickerView
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 250/255, green: 150/255, blue: 10/255, alpha: 1)
        doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(CashUpViewController.donePressed))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CashUpViewController.cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        pickerView.backgroundColor = UIColor .white
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        locationTxt?.inputAccessoryView = toolBar
        terminalTxt?.inputAccessoryView = toolBar
       // productSixthTxt?.inputAccessoryView = toolBar
        
//       locationTxt?.tintColor = UIColor.clear
//         terminalTxt?.tintColor = UIColor.clear
//         productSixthTxt?.tintColor = UIColor.clear
        
        for i in 0 ..< 10
        {
            let val = String(i)
            locationItemArr.add("Greenlane"+val)
            terminalItemArr.add("Terminal"+val)
            saveItemArr.add("PDF"+val)
        }
        
        notesItemsPos()
    }
    func donePressed (sender: UIButton) {
        locationTxt?.isUserInteractionEnabled = true
        
       if fieldTagVal == 6
        {
            locationTxt?.text = pickOption[sender.tag] as? String
        }
        else
        {
            terminalTxt?.text = pickOption[sender.tag] as? String
        }
        
        view.endEditing(true)
        
    }
    
    func cancelPressed() {
        locationTxt?.isUserInteractionEnabled = true
        view.endEditing(true) // or do something
    }
    public func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //print("pickOption.count\( pickOption.count)")
        
        return pickOption.count
        
    }
    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        // print( "The value is\(pickOption[row])")
//        
//        return pickOption[row] as? String
//    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: (pickOption[row] as? String)!, attributes: [NSForegroundColorAttributeName : UIColor (colorLiteralRed: 66/255, green: 0/255, blue: 0/255, alpha: 1)])
        return attributedString
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        doneButton.tag = row
    }
    @IBAction func countButtonAction(sender : UIButton)
    {
        contentView?.isHidden = false
    }
    @IBAction func submitButtonAction(sender : UIButton)
    {
        contentView?.isHidden = true
        notesItemsPos()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    @IBAction func notesButtonAction(sender : UIButton)
    {
        notesItemsPos()
    }
    func notesItemsPos()
    {
        self.submitButton?.isHidden = true
        notesUnderLine?.isHidden = false
        coinsUnderLine?.isHidden = true
        totalUnderLine?.isHidden = true
        self.saveButton?.isHidden = true
        self.showFieldData()
        self.submitButton?.setTitle("Submit", for: UIControlState.normal)
        let when = DispatchTime.now() + 0.2
        DispatchQueue.main.asyncAfter(deadline: when){
            self.submitButton?.isHidden = false
            self.productOneLbl?.text = "$5's x"
            self.productSecondLbl?.text = "$10's x"
            self.productThirdLbl?.text = "$20's x"
            self.productFourthLbl?.text = "$50's x"
            self.productFifthLbl?.text = "$10's x"
            
            self.productOneLbl?.isHidden = false
            self.productSecondLbl?.isHidden = false
            self.productThirdLbl?.isHidden = false
            self.productFourthLbl?.isHidden = false
            self.productFifthLbl?.isHidden = false
            self.productSixthLbl?.isHidden = true
            
            self.productOneTxt?.isHidden = false
            self.productSecondTxt?.isHidden = false
            self.productThirdTxt?.isHidden = false
            self.productFourthTxt?.isHidden = false
            self.productFifthTxt?.isHidden = false
            
            self.productOneUndLine?.isHidden = false
            self.productSecondUndLine?.isHidden = false
            self.productThirdUndLine?.isHidden = false
            self.productFourthUndLine?.isHidden = false
            self.productFifthUndLine?.isHidden = false
            self.productSixthUndLine?.isHidden = true
            self.saveDropDownButton?.isHidden = true
            
            
            
            
            self.submitButton?.frame.origin.y = (self.productFifthUndLine?.frame.origin.y)! + 20
        }
    }
    @IBAction func coinsButtonAction(sender : UIButton)
    {
        self.submitButton?.isHidden = true
        self.notesUnderLine?.isHidden = true
        self.coinsUnderLine?.isHidden = false
        self.saveButton?.isHidden = true
        self.totalUnderLine?.isHidden = true
        self.showFieldData()
        self.submitButton?.setTitle("Submit", for: UIControlState.normal)
        let when = DispatchTime.now() + 0.2
        DispatchQueue.main.asyncAfter(deadline: when){
            self.submitButton?.isHidden = false
            
            self.productOneLbl?.text = ".10's x"
            self.productSecondLbl?.text = ".50's x"
            self.productThirdLbl?.text = ".1's x"
            self.productFourthLbl?.text = ".2's x"
            
            self.productOneLbl?.isHidden = false
            self.productSecondLbl?.isHidden = false
            self.productThirdLbl?.isHidden = false
            self.productFourthLbl?.isHidden = false
            self.productFifthLbl?.isHidden = true
            self.productSixthLbl?.isHidden = true
            
            self.productOneTxt?.isHidden = false
            self.productSecondTxt?.isHidden = false
            self.productThirdTxt?.isHidden = false
            self.productFourthTxt?.isHidden = false
            self.productFifthTxt?.isHidden = true
            
            self.productOneUndLine?.isHidden = false
            self.productSecondUndLine?.isHidden = false
            self.productThirdUndLine?.isHidden = false
            self.productFourthUndLine?.isHidden = false
            self.productFifthUndLine?.isHidden = true
            self.productSixthUndLine?.isHidden = true
            self.saveDropDownButton?.isHidden = true
            
            self.submitButton?.frame.origin.y = (self.productFourthUndLine?.frame.origin.y)! + 20
        }
    }
    @IBAction func totalButtonAction(sender : UIButton)
    {
        self.submitButton?.isHidden = true
        self.notesUnderLine?.isHidden = true
        self.coinsUnderLine?.isHidden = true
        self.saveButton?.isHidden = false
        self.totalUnderLine?.isHidden = false
        self.showFieldData()
        self.submitButton?.setTitle("Confirm", for: UIControlState.normal)
        let when = DispatchTime.now() + 0.2
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            self.submitButton?.isHidden = false
            
            
            self.productOneLbl?.isHidden = false
            self.productSecondLbl?.isHidden = false
            self.productThirdLbl?.isHidden = false
            self.productFourthLbl?.isHidden = false
            self.productFifthLbl?.isHidden = false
            self.productSixthLbl?.isHidden = false
            
            self.productOneTxt?.isHidden = false
            self.productSecondTxt?.isHidden = false
            self.productThirdTxt?.isHidden = false
            self.productFourthTxt?.isHidden = false
            self.productFifthTxt?.isHidden = false
            
            
            self.productOneUndLine?.isHidden = false
            self.productSecondUndLine?.isHidden = false
            self.productThirdUndLine?.isHidden = false
            self.productFourthUndLine?.isHidden = false
            self.productFifthUndLine?.isHidden = false
            self.productSixthUndLine?.isHidden = false
            
            
            self.productOneLbl?.text = "Coins"
            self.productSecondLbl?.text = "NOTES"
            self.productThirdLbl?.text = "LESS FLOAT"
            self.productFourthLbl?.text = "SYSTEM TOTAL"
            self.productFifthLbl?.text = "VARIANCE"
            self.productSixthLbl?.text = "SAVE"
            self.saveDropDownButton?.isHidden = false
            
            self.submitButton?.frame.origin.y = ( self.saveButton?.frame.origin.y)! + 70
        }
    }
    @IBAction func backButtonAction(sender : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    func showFieldData()
    {
        self.productOneTxt?.text = "15"
        self.productSecondTxt?.text = "15"
        self.productThirdTxt?.text = "15"
        self.productFourthTxt?.text = "15"
        self.productFifthTxt?.text = "15"
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {

        if textField.tag == 0
        {
            productOneUndLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 1
        {
            productSecondUndLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 2
        {
            productThirdUndLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 3
        {
            productFourthUndLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 4
        {
            productFifthUndLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 6
        {
            fieldTagVal = 6
             pickOption = NSMutableArray(array: locationItemArr)
            locationUndLine?.backgroundColor = UIColor.lightGray
            
        }
        else  if textField.tag == 7
        {
            fieldTagVal = 7
         pickOption = NSMutableArray(array: terminalItemArr)
            
            terminalUndLine?.backgroundColor = UIColor.lightGray
        }
        pickerView.reloadAllComponents()
          pickerView.selectedRow(inComponent: 0)
        
    }
        @IBAction func savePhotoAction(sender : UIButton)
        {
            let strMatch = (saveButton?.currentTitle)! as NSString
            if  strMatch .isEqual(to: "Photo")
            {
            openCamera()
            }
            else
            {
                
            }
        //Create the UIImage
//        let view = self.view
//        let scale = UIScreen.main.scale
//        UIGraphicsBeginImageContextWithOptions((view?.bounds.size)!, false, scale);
//        contentView?.layer.render(in: UIGraphicsGetCurrentContext()!)
//        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        UIImageWriteToSavedPhotosAlbum(screenshot!, nil, nil, nil)
//        alertBar()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 0
        {
            productOneUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 1
        {
            productSecondUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 2
        {
            productThirdUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 3
        {
            productFourthUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 4
        {
            productFifthUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 5
        {
            productSixthUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 6
        {
            locationUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 7
        {
            terminalUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }

    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
         let allowedCharacters = CharacterSet.decimalDigits
         let characterSet = CharacterSet(charactersIn: string)
        
        if textField.tag == 6
        {}
        else if textField.tag == 7
        {}
        else
        {
        return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
           // imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alertView = UIAlertController(title: "Warning", message: "Camera not available", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            alertView.view.tintColor=UIColor.blue
            
            present(alertView, animated: true, completion: nil)
            
        }
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
      //  saveButton?.setTitle("Save",for: .normal)
        clickedImg = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        let imageData = UIImageJPEGRepresentation(clickedImg, 0.6)
        let compressedJPGImage = UIImage(data: imageData!)
     //   UIImageWriteToSavedPhotosAlbum(compressedJPGImage!, nil, nil, nil)
        SaveImage.sharedInstance.save(image:compressedJPGImage!)
        self.dismiss(animated: true, completion: nil)
        alertBar()
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //print("Cancel")
        dismiss(animated:true, completion: nil)
    }
    func alertBar() {
        let alertView = UIAlertController(title: nil, message:"Photo saved successfully" as String , preferredStyle: UIAlertControllerStyle.alert)
        self.present(alertView, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alertView.dismiss(animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
