//
//  LogInSignUpViewController.swift
//  CoffeLab
//
//  Created by Brst on 21/08/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class LogInSignUpViewController: UIViewController,UITextFieldDelegate,UIScrollViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    @IBOutlet weak var userNameTxtFiled = UITextField()
    @IBOutlet weak var passwordTxtFiled = UITextField()
    @IBOutlet weak var logInButton = UIButton()
    @IBOutlet weak var logInUderLine = UILabel()
    @IBOutlet weak var signUpButton = UIButton()
    @IBOutlet weak var signUpUderLine = UILabel()
    @IBOutlet weak var signInButton = UIButton()
    @IBOutlet weak var forgotPassButton = UIButton()
    @IBOutlet weak var userNameUderLine = UILabel()
    @IBOutlet weak var passwordUderLine = UILabel()
    @IBOutlet weak var firstNameTxt = UITextField()
    @IBOutlet weak var lastNameTxt = UITextField()
    @IBOutlet weak var emailText = UITextField()
    @IBOutlet weak var signUpPassText = UITextField()
    @IBOutlet weak var cafeCodeTxt = UITextField()
    
    @IBOutlet weak var firstNameUderLine = UILabel()
    @IBOutlet weak var lastNameUderLine = UILabel()
    @IBOutlet weak var emailUderLine = UILabel()
    @IBOutlet weak var signUpPassUderLine = UILabel()
    @IBOutlet weak var cafeCodeUderLine = UILabel()
    @IBOutlet weak var signUpContentView = UIView()
    @IBOutlet weak var userImgView = UIButton()
    @IBOutlet weak var signUpDataButton = UIButton()
    @IBOutlet weak var itemsScrollView = UIScrollView()
    @IBOutlet weak var profileImgView = UIImageView()
    @IBOutlet weak var facebookButton = UIButton()
     var strModelName = NSString()
    var alertMsg = String()
    
    var pickerController = UIImagePickerController()
    var profileImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let modelName = UIDevice.current.model
        
        strModelName = String(modelName) as NSString
        if strModelName .isEqual(to: "iPhone")
        {
            if self.view.frame.size.height > 700
            {
                profileImgView?.frame.size.height = 93
                profileImgView?.frame.size.width = 93
            }
    
         
        }
        else
        {
            profileImgView?.frame.size.height = 125
            profileImgView?.frame.size.width = 125
        }
        signUpContentView?.isHidden = true
        signUpUderLine?.isHidden = true
        userNameTxtFiled?.delegate = self
        passwordTxtFiled?.delegate = self
        firstNameTxt?.delegate = self
        lastNameTxt?.delegate = self
        emailText?.delegate = self
        signUpPassText?.delegate = self
        cafeCodeTxt?.delegate = self
        itemsScrollView?.delegate = self
      
        userNameTxtFiled?.tag = 0
        passwordTxtFiled?.tag = 1
        firstNameTxt?.tag = 2
        lastNameTxt?.tag = 3
        emailText?.tag = 4
        signUpPassText?.tag = 5
        cafeCodeTxt?.tag = 6
        
        
        logInUderLine?.frame.size.height = 1.5
        signUpUderLine?.frame.size.height = 1.5
        userNameUderLine?.frame.size.height = 1
        passwordUderLine?.frame.size.height = 1
        signInButton?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
        signInButton?.layer.borderWidth = 1.5
        signInButton?.layer.cornerRadius = 20
        signUpDataButton?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
        signUpDataButton?.layer.borderWidth = 1.5
        signUpDataButton?.layer.cornerRadius = 20
        
        self.itemsScrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:650)
        
        
        if UserDefaults.standard.object(forKey: "UserImage") != nil
        {
            profileImgView?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
            let imageData: Data? = UserDefaults.standard.object(forKey: "UserImage") as! Data?
            let image = UIImage(data: imageData!)
            profileImgView?.image = image
        }
        else
        {
            profileImgView?.layer.borderColor = UIColor .lightGray .cgColor
        }
        profileImgView?.layer.masksToBounds = false
        profileImgView?.layer.cornerRadius = (profileImgView?.frame.size.height)!/2
        profileImgView?.layer.borderWidth = 1
        
        profileImgView?.clipsToBounds = true
        
    }
    @IBAction func logInAction (sender: UIButton)
    {
        self.itemsScrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:650)
        logInUderLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        signUpUderLine?.backgroundColor = UIColor .clear
        signUpUderLine?.isHidden = true
        logInUderLine?.isHidden = false
        signUpContentView?.isHidden = true
    }
    @IBAction func signUpAction (sender: UIButton)
    {
        if strModelName .isEqual(to: "iPhone")
        {
        self.itemsScrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:self.view.frame.size.height+50)
        }
        else
        {
           self.itemsScrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:1200)
        }
        signUpUderLine?.isHidden = false
        logInUderLine?.isHidden = true
        signUpContentView?.isHidden = false
        
        logInUderLine?.backgroundColor = UIColor .clear
        signUpUderLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
    }
    @IBAction func signInAction (sender: UIButton)
    {
        //        if (userNameTxtFiled?.text?.isEmpty)!
        //        {
        //            alertMsg = "Please enter username"
        //            alertBar()
        //        }
        //        else if (passwordTxtFiled?.text?.isEmpty)!
        //        {
        //            alertMsg = "Please enter password"
        //            alertBar()
        //        }
        //        else
        //
        //        {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "SSASideMenu") as! SSASideMenu
        self.navigationController?.pushViewController(nextViewController, animated: true)
        // }
    }
    @IBAction func facebookBtnAction (sender: UIButton)
    {
        
    }
    @IBAction func forgotPassBtnAction (sender: UIButton)
    {
        
    }
    @IBAction func signUpDataButtonAction (sender: UIButton)
    {
        if strModelName .isEqual(to: "iPhone")
        {
            self.itemsScrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:650)
        }
        else
        {
            self.itemsScrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:500)
        }
        firstNameTxt?.text = ""
        lastNameTxt?.text = ""
        emailText?.text = ""
        cafeCodeTxt?.text = ""
        signUpPassText?.text = ""
        signUpUderLine?.isHidden = true
        logInUderLine?.isHidden = false
        logInUderLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        signUpContentView?.isHidden = true
    }
    @IBAction func changeProfileAction (sender: UIButton)
    {
       changeProfile()
    }

    func changeProfile()
    {
        let alertController: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        alertController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Open Camera", style: .default)
        { _ in
            //print("Open Camera")
            self.openCamera()
        }
        alertController.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Open Gallery", style: .default)
        { _ in
            //print("Open Gallery")
            self.openGallary()
        }
        alertController.addAction(deleteActionButton)
        
        alertController.popoverPresentationController?.sourceView = self.view
        
        alertController.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.size.width / 2.0,y: self.view.bounds.size.height / 2.0,width: 1.0,height: 1.0)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        profileImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        profileImgView?.image = profileImage
        profileImgView?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
        UserDefaults.standard.set(UIImagePNGRepresentation( (profileImgView?.image)!), forKey: "UserImage")
        self.dismiss(animated: true, completion: nil)
  
        
    }

    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //print("Cancel")
        dismiss(animated:true, completion: nil)
    }

    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openGallary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
      
        if textField.tag == 0
        {
            userNameUderLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 1
        {
            passwordUderLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 2
        {
            firstNameUderLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 3
        {
            lastNameUderLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 4
        {
            emailUderLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 5
        {
            signUpPassUderLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 6
        {
            cafeCodeUderLine?.backgroundColor = UIColor.lightGray
        }
        
    }
   
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        userNameTxtFiled?.resignFirstResponder()
        passwordTxtFiled?.resignFirstResponder()
        
        if textField.tag == 0 {
            userNameUderLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 1
        {
            passwordUderLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 2
        {
            firstNameUderLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 3
        {
            lastNameUderLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 4
        {
            emailUderLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 5
        {
            signUpPassUderLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 6
        {
            cafeCodeUderLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
  
    }
   
    func alertBar() {
        let alertView = UIAlertController(title: nil, message:alertMsg as String , preferredStyle: UIAlertControllerStyle.alert)
        self.present(alertView, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alertView.dismiss(animated: true, completion: nil)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        firstNameTxt?.resignFirstResponder()
        lastNameTxt?.resignFirstResponder()
        emailText?.resignFirstResponder()
        passwordTxtFiled?.resignFirstResponder()
        signUpPassText?.resignFirstResponder()
        cafeCodeTxt?.resignFirstResponder()
        userNameTxtFiled?.resignFirstResponder()
        return true
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.bounces = (scrollView.contentOffset.y > 50);
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
