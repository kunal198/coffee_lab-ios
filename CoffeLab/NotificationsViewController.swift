//
//  NotificationsViewController.swift
//  SideBarMenu-Swift
//
//  Created by Wim on 8/6/16.
//  Copyright © 2016 Kwikku. All rights reserved.
//

import Foundation

class NotificationsViewController: UIViewController {
    
    @IBOutlet weak var menuBar: UIBarButtonItem!
    
    override func viewDidLoad() {
        setMenuBarBtn(menuBar)
    }
    
    func setMenuBarBtn(_ menuBar: UIBarButtonItem) {
        menuBar.target = revealViewController()
        menuBar.action = #selector(SWRevealViewController.revealToggle(_:))
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
}
