//
//  BakedGoodsViewController.swift
//  CoffeLab
//
//  Created by Brst on 21/08/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class BakedGoodsViewController: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIScrollViewDelegate {
    
    @IBOutlet weak var countButton = UIButton()
    @IBOutlet weak var backButton = UIButton()
    @IBOutlet weak var backIcon = UIImageView()
    @IBOutlet weak var submitButton = UIButton()
    @IBOutlet weak var itemsScrollView = UIScrollView()
    @IBOutlet weak var friandasUnderLine = UILabel()
    
    @IBOutlet weak var quatyCarmtxt = UITextField()
    @IBOutlet weak var chocoCarmtxt = UITextField()
    @IBOutlet weak var chocoBrowtxt = UITextField()
    @IBOutlet weak var gingerCrutxt = UITextField()
    @IBOutlet weak var afgansTxt = UITextField()
    @IBOutlet weak var yoyosTxt = UITextField()
    @IBOutlet weak var qatyCooTxt = UITextField()
    @IBOutlet weak var florentineTxt = UITextField()
    @IBOutlet weak var orangeAlmTxt = UITextField()
    @IBOutlet weak var rawCarmtxt = UITextField()
    @IBOutlet weak var rawBerrTxt = UITextField()
    @IBOutlet weak var berryBlissTxt = UITextField()
    @IBOutlet weak var espressoBlisTxt = UITextField()
    @IBOutlet weak var friandasTxt = UITextField()
    
    @IBOutlet weak var quatyCarmLine = UILabel()
    @IBOutlet weak var chocoCarmLine = UILabel()
    @IBOutlet weak var chocoBrowLine = UILabel()
    @IBOutlet weak var gingerCruLine = UILabel()
    @IBOutlet weak var afgansLine = UILabel()
    @IBOutlet weak var yoyosLine = UILabel()
    @IBOutlet weak var qatyCooLine = UILabel()
    @IBOutlet weak var florentineLine = UILabel()
    @IBOutlet weak var orangeAlmLine = UILabel()
    @IBOutlet weak var rawCarmLine = UILabel()
    @IBOutlet weak var rawBerrLine = UILabel()
    @IBOutlet weak var berryBlissLine = UILabel()
    @IBOutlet weak var espressoBlisLine = UILabel()
    @IBOutlet weak var friandasLine = UILabel()
    @IBOutlet weak var scrollBackView = UIView()
    
    @IBOutlet weak var locationTxt = UITextField()
    @IBOutlet weak var timeDeliveryTxt = UITextField()
    @IBOutlet weak var locationUndLine = UILabel()
    @IBOutlet weak var timeDeliveryUndLine = UILabel()
    @IBOutlet weak var headerLabel = UILabel()
    var fieldTagVal = Int()
    var doneButton: UIBarButtonItem!
    var pickOption :NSMutableArray = NSMutableArray()
    let pickerView = UIPickerView()
    var locationItemArr = NSMutableArray()
    var terminalItemArr = NSMutableArray()
    var strModelName = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
        let modelName = UIDevice.current.model
        
        strModelName = String(modelName) as NSString
        itemsScrollView?.isHidden = true
        scrollBackView?.isHidden = true
        countButton?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
        countButton?.layer.borderWidth = 1.5
        countButton?.layer.cornerRadius = 20
        submitButton?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
        submitButton?.layer.borderWidth = 1.5
        submitButton?.layer.cornerRadius = 20
        
        backIcon?.image = backIcon?.image!.withRenderingMode(.alwaysTemplate)
        backIcon?.tintColor = UIColor (colorLiteralRed: 66/255, green: 0/255, blue: 0/255, alpha: 1)
        itemsScrollView?.delegate = self
        quatyCarmtxt?.delegate = self
        chocoCarmtxt?.delegate = self
        chocoBrowtxt?.delegate = self
        gingerCrutxt?.delegate = self
        afgansTxt?.delegate = self
        yoyosTxt?.delegate = self
        qatyCooTxt?.delegate = self
        florentineTxt?.delegate = self
        orangeAlmTxt?.delegate = self
        rawCarmtxt?.delegate = self
        rawBerrTxt?.delegate = self
        berryBlissTxt?.delegate = self
        espressoBlisTxt?.delegate = self
        friandasTxt?.delegate = self
        locationTxt?.delegate = self
          timeDeliveryTxt?.delegate = self
        
        quatyCarmtxt?.tag = 0
        chocoCarmtxt?.tag = 1
        chocoBrowtxt?.tag = 2
        gingerCrutxt?.tag = 3
        afgansTxt?.tag = 4
        yoyosTxt?.tag = 5
        qatyCooTxt?.tag = 6
        florentineTxt?.tag = 7
        orangeAlmTxt?.tag = 8
        rawCarmtxt?.tag = 9
        rawBerrTxt?.tag = 10
        berryBlissTxt?.tag = 11
        espressoBlisTxt?.tag = 12
        friandasTxt?.tag = 13
        locationTxt?.tag = 14
        timeDeliveryTxt?.tag = 15
        
        if strModelName .isEqual(to: "iPhone")
        {
            self.itemsScrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:((self.itemsScrollView?.frame.size.height)!+500))
        }
        else
        {
            self.itemsScrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:((self.itemsScrollView?.frame.size.height)!+700))
        }
        
        pickerView.delegate = self
        locationTxt?.inputView = pickerView
        timeDeliveryTxt?.inputView = pickerView
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 250/255, green: 150/255, blue: 10/255, alpha: 1)
        doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(BakedGoodsViewController.donePressed))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(BakedGoodsViewController.cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
      pickerView.backgroundColor = UIColor .white
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        locationTxt?.inputAccessoryView = toolBar
        timeDeliveryTxt?.inputAccessoryView = toolBar
        
        for i in 0 ..< 10
        {
            let val = String(i)
            locationItemArr.add("Greenlane"+val)
            terminalItemArr.add("AM"+val)
        }
        itemsScrollView?.showsVerticalScrollIndicator = false
    }
    func donePressed (sender: UIButton) {
        locationTxt?.isUserInteractionEnabled = true
        if fieldTagVal == 14
        {
            locationTxt?.text = pickOption[sender.tag] as? String
        }
        else
        {
            timeDeliveryTxt?.text = pickOption[sender.tag] as? String
        }
        
        view.endEditing(true)
        
    }
    
    func cancelPressed() {
        locationTxt?.isUserInteractionEnabled = true
        view.endEditing(true) // or do something
    }
    public func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
        
    }
    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        // print( "The value is\(pickOption[row])")
//        return pickOption[row] as? String
//    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: (pickOption[row] as? String)!, attributes: [NSForegroundColorAttributeName : UIColor (colorLiteralRed: 66/255, green: 0/255, blue: 0/255, alpha: 1)])
        return attributedString
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        doneButton.tag = row
    }
    @IBAction func countButtonAction(sender : UIButton)
    {
        itemsScrollView?.isHidden = false
        scrollBackView?.isHidden = false
    }
    @IBAction func submitButtonAction(sender : UIButton)
    {
        itemsScrollView?.isHidden = true
        scrollBackView?.isHidden = true
    }
    @IBAction func backButtonAction(sender : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        pickOption.removeAllObjects()
        self.itemsScrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:((self.itemsScrollView?.frame.size.height))!)
        if strModelName .isEqual(to: "iPhone")
        {
            self.itemsScrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:((self.itemsScrollView?.frame.size.height)!+500))
        }
        else
        {
            self.itemsScrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:((self.itemsScrollView?.frame.size.height)!+700))
        }
        
        if textField.tag == 0
        {
            quatyCarmLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 1
        {
            chocoCarmLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 2
        {
            chocoBrowLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 3
        {
            gingerCruLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 4
        {
            afgansLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 5
        {
            yoyosLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 6
        {
            qatyCooLine?.backgroundColor = UIColor.lightGray
        }
        
        if textField.tag == 7
        {
            florentineLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 8
        {
            orangeAlmLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 9
        {
            rawCarmLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 10
        {
            rawBerrLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 11
        {
            berryBlissLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 12
        {
            espressoBlisLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 13
        {
            friandasLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 14
        {
            fieldTagVal = 14
            pickOption = NSMutableArray(array: locationItemArr)
            
            
            locationUndLine?.backgroundColor = UIColor.lightGray
            
        }
        else if textField.tag == 15
        {
            fieldTagVal = 15
            pickOption = NSMutableArray(array: terminalItemArr)
            timeDeliveryUndLine?.backgroundColor = UIColor.lightGray
        }
        pickerView.selectedRow(inComponent: 0)
        pickerView.reloadAllComponents()
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
     //pickOption.removeAllObjects()
        if textField.tag == 0
        {
            quatyCarmLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
            
        }
        else if textField.tag == 1
        {
            chocoCarmLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 2
        {
            chocoBrowLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 3
        {
            gingerCruLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 4
        {
            afgansLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 5
        {
            yoyosLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 6
        {
            qatyCooLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        
        if textField.tag == 7
        {
            florentineLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 8
        {
            orangeAlmLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 9
        {
            rawCarmLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 10
        {
            rawBerrLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 11
        {
            berryBlissLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 12
        {
            espressoBlisLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 13
        {
            friandasLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 14
        {
            locationUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
        else if textField.tag == 15
        {
            timeDeliveryUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }

    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        
        if textField.tag == 14
        {}
        else if textField.tag == 15
        {}
        else
        {
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.bounces = (scrollView.contentOffset.y > 50);
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
