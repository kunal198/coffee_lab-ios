//
//  HomeViewController.swift
//  CoffeLab
//
//  Created by Brst on 21/08/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit
class HomeViewController: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var logOutButton = UIButton()
    @IBOutlet weak var optionsMenuButton = UIButton()
    @IBOutlet weak var menuImgView = UIImageView()
    @IBOutlet weak var coffeCountlbl = UILabel()
    @IBOutlet weak var menuBar: UIBarButtonItem!
    var strModelName = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
        let modelName = UIDevice.current.model
        strModelName = String(modelName) as NSString
        optionsMenuButton?.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), for: .touchUpInside)
        menuImgView?.image = menuImgView?.image!.withRenderingMode(.alwaysTemplate)
        menuImgView?.tintColor = UIColor (colorLiteralRed: 66/255, green: 0/255, blue: 0/255, alpha: 1)
    }
      @IBAction func coffeeCountAction(sender : UIButton)
    {
               let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "CoffeeCountViewController") as! CoffeeCountViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func cashUpAction(sender : UIButton)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "CashUpViewController") as! CashUpViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func bakedGoodsAction(sender : UIButton)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "BakedGoodsViewController") as! BakedGoodsViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func temperatureAction(sender : UIButton)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "TemperatureViewController") as! TemperatureViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func storeRoomSuppAction(sender : UIButton)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "StoreRoomSuppViewController") as! StoreRoomSuppViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func tasksAction(sender : UIButton)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "TasksViewController") as! TasksViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
