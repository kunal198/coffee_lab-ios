//
//  TasksViewController.swift
//  CoffeLab
//
//  Created by Brst on 21/08/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class TasksViewController: UIViewController,UITextFieldDelegate,UIPickerViewDelegate {
    
    @IBOutlet weak var confirmButton = UIButton()
    @IBOutlet weak var backButton = UIButton()
    @IBOutlet weak var backIcon = UIImageView()
    @IBOutlet weak var dropDownButton = UIButton()
    @IBOutlet weak var locationUndLine = UILabel()
    @IBOutlet weak var taskUndLine = UILabel()
    @IBOutlet weak var locationTxt = UITextField()
    @IBOutlet weak var taskTxt = UITextField()
    var doneButton: UIBarButtonItem!
    var pickOption :NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        confirmButton?.layer.borderColor = UIColor(patternImage: UIImage(named: "header.png")!) .cgColor
        confirmButton?.layer.borderWidth = 1
        confirmButton?.layer.cornerRadius = 20
        backIcon?.image = backIcon?.image!.withRenderingMode(.alwaysTemplate)
        backIcon?.tintColor = UIColor (colorLiteralRed: 66/255, green: 0/255, blue: 0/255, alpha: 1)
        locationTxt?.delegate = self
        taskTxt?.delegate = self
        locationTxt?.tag = 0
        taskTxt?.tag = 1
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        locationTxt?.inputView = pickerView
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 250/255, green: 150/255, blue: 10/255, alpha: 1)
        doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(TasksViewController.donePressed))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(TasksViewController.cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
         pickerView.backgroundColor = UIColor .white
        locationTxt?.delegate = self
        locationTxt?.inputAccessoryView = toolBar
        
        
        for i in 0 ..< 10
        {
            let val = String(i)
            pickOption.add("Greenlane"+val)
        }
        
    }
    func donePressed (sender: UIButton) {
        locationTxt?.isUserInteractionEnabled = true
        //let valueSelected = pickOption[sender.tag] as! String
       // let val = String(describing: valueSelected)
        locationTxt?.text = pickOption[sender.tag] as? String
        view.endEditing(true)
    }
    
    func cancelPressed() {
        locationTxt?.isUserInteractionEnabled = true
        view.endEditing(true) // or do something
    }
    public func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
        
    }
    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//       // print( "The value is\(pickOption[row])")
//        return pickOption[row] as? String
//    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: (pickOption[row] as? String)!, attributes: [NSForegroundColorAttributeName : UIColor (colorLiteralRed: 66/255, green: 0/255, blue: 0/255, alpha: 1)])
        return attributedString
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        doneButton.tag = row
    }

    @IBAction func confirmButtonAction(sender: UIButton)
    {
        
    }
    @IBAction func dropDownAction(sender: UIButton)
    {
        
    }
    @IBAction func backButtonAction(sender : UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 0
        {
            locationUndLine?.backgroundColor = UIColor.lightGray
        }
        else if textField.tag == 1
        {
            taskUndLine?.backgroundColor = UIColor.lightGray
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {

        
        if textField.tag == 0
        {
            locationUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
            
        }
        else if textField.tag == 1
        {
            taskUndLine?.backgroundColor = UIColor(patternImage: UIImage(named: "header.png")!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
